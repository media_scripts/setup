FROM debian
MAINTAINER Yusuf Ali "ali@yusuf.email"

ADD root/ /root/
RUN /root/setup.sh

CMD ["/bin/bash"]