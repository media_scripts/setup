source /etc/profile.d/media_env.sh
set -e

# check if plex is installed
service plexmediaserver status > /dev/null
if [ "$?" -ne "0" ]; then echo "Plex not installed" && exit 1; fi

# check if docker is installed
if [ ! -x "$(which docker)" ]; then echo "Docker not installed" && exit 1; fi
if [ ! -d $CONFIG/docker ]; then "docker was not cloned" && exit 1; fi

# check if rclone is installed
if [ ! -x "$(which rclone)" ]; then echo "Rclone not installed" && exit 1; fi

# check if google ocaml is installed
if [ ! -x "$(which google-drive-ocamlfuse)" ]; then echo "google ocmal not installed" && exit 1; fi

# check if $CONFIG exists
if [ ! -d "$CONFIG" ]; then echo "config dir no exist" && exit 1; fi

# check if installed media tools are working
if [ ! -d ~plex/Library ]; then "plex isn't working" && exit 1; fi
if [ ! -d ~plex/python ]; then "plex scripts folder is lost" && exit 1; fi
if [ ! -d ~plex/data ]; then "plex data folder is lost" && exit 1; fi
if [ ! -d "$CONFIG/scripts" ]; then "scripts didnt copy over" && exit 1; fi

