set -e 

if [ "$(whoami)" == "root" ]; then
  apt-get update 
  if [ ! -x "$(which sudo)" ]; then
    apt-get install -y sudo
  fi
  apt-get install -y ca-certificates
else
  if [ ! -x "$(which sudo)" ]; then
    echo "Sudo not installed!, need elevated permissions"
    exit 1;
  else
    sudo apt-get install -y ca-certificates
  fi 
fi

# install git if required
if [ ! -x "$(which git)" ]; then
  sudo apt-get update
  sudo apt-get install -y git
fi

# install wget if required
if [ ! -x "$(which wget)" ]; then
  sudo apt-get update
  sudo apt-get install -y wget 
fi

# clone repo
if [[ -v CI_COMMIT_REF_SLUG ]]; then
  git clone -b $CI_COMMIT_REF_SLUG --depth=1 https://github.com/FlimPlayer/setup.git /tmp/setup
elif [ -d /tmp/setup ]; then
  echo "Using travis build files"
else
  git clone --depth=1 https://github.com/FlimPlayer/setup.git /tmp/setup
fi
sudo mv /tmp/setup/root/* /root/.
sudo /root/setup.sh
source /etc/profile.d/media_env.sh

# plex user
/etc/init.d/plexmediaserver stop
git clone --depth=1 https://github.com/FlimPlayer/plex.git /tmp/plex 
sudo -H -u plex bash -c "cp -r /tmp/plex/* ~plex/."
sudo -H -u plex bash -c "cp -r /tmp/plex/.git ~plex/."
sudo -H -u plex bash -c "mkdir -p ~plex/data && touch ~plex/data/to_scan.txt"
sudo -H -u plex bash -c "crontab ~plex/python/cron"

# media user 
sudo -H -u media bash -c "git clone --depth=1 https://github.com/FlimPlayer/scripts.git $CONFIG/scripts"
sudo -H -u media bash -c "git clone --depth=1 https://github.com/FlimPlayer/docker.git $CONFIG/docker"
sudo -H -u media bash -c "crontab $CONFIG/scripts/cron/media.cron"

# install root crontab
sudo git clone https://github.com/FlimPlayer/root_cron.git /root/root_cron
sudo crontab /root/root_cron/crontab

