# FlimPlayer [![Build Status](https://travis-ci.org/FlimPlayer/FlimPlayer.svg?branch=master)](https://travis-ci.org/FlimPlayer/FlimPlayer)

A home theater PC system (HTPC) that combines the usage of Plex, rclone, and docker to build the ultimate media solution. 

## Installation

```
/bin/bash -c "$(wget https://raw.githubusercontent.com/FlimPlayer/FlimPlayer/master/setup.sh -O -)"
```


## Features
* Essential media tools already preconfigured and ready to go! 
    * **media managers**: Plex, sonarr, couchpotato, headphones, mylar
    * **downloaders**: deluge, sabnzbd
* [rclone][1] integration to incorporate your favorite cloud providers!
* Robust scripts to automatically manage your HTPC!
    * automatically encode/mux media to be web-streamable.
    * Plex library caching to ensure you never hit your [rclone][1] API limits.
    * Plex library integrity checking to make sure your files never *slip* out of your library.

## Supported Systems

Currently only supports debian 9.

[1]: https://rclone.org/
