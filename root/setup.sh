#!/bin/bash

set -e
apt-get update
apt-get upgrade -y
apt-get remove -y --purge apache2 screen nano
apt-get install -y p7zip-full vim ffmpeg mediainfo zsh curl tmux build-essential make wget atomicparsley perl python3 fuse python3-pip opam ocaml-nox make camlp4-extra unzip unionfs-fuse pkg-config nodejs git gnupg locales nfs-common sudo inotify-tools udev
pip3 install setuptools
pip3 install docker-compose pymongo pymediainfo qtfaststart

# set locales and enable uft-8
  echo "LC_ALL=en_US.UTF-8" >> /etc/environment
  echo "en_US.UTF-8 UTF-8" >> /etc/locale.gen
  echo "LANG=en_US.UTF-8" > /etc/locale.conf
  locale-gen en_US.UTF-8

# install docker
  apt-get install -y apt-transport-https ca-certificates curl gnupg2 software-properties-common
  curl -fsSL https://download.docker.com/linux/$(. /etc/os-release; echo "$ID")/gpg | apt-key add -
  apt-key fingerprint 0EBFCD88
  add-apt-repository \
    "deb [arch=amd64] https://download.docker.com/linux/$(. /etc/os-release; echo "$ID") \
    $(lsb_release -cs) \
    stable" 
  apt-get update
  apt-get install -y docker-ce

# install rclone
  cd /root/
  wget https://downloads.rclone.org/rclone-current-linux-amd64.zip
  unzip rclone-current-linux-amd64.zip
  cd rclone-*-linux-amd64
  cp rclone /usr/bin/
  chown root:root /usr/bin/rclone
  chmod 755 /usr/bin/rclone
  mkdir -p /usr/local/share/man/man1
  cp rclone.1 /usr/local/share/man/man1/

# install plex
  cd /root
  echo "deb http://shell.ninthgate.se/packages/debian wheezy main" | tee -a /etc/apt/sources.list.d/plexmediaserver.list
  curl http://shell.ninthgate.se/packages/shell.ninthgate.se.gpg.key | apt-key add -
  apt-get update
  apt-get install -y plexmediaserver
  rm /etc/apt/sources.list.d/plexmediaserver.list
  /etc/init.d/plexmediaserver stop
  rm -rf ~plex/Library
  wget https://github.com/FlimPlayer/FlimPlayer/raw/master/root/plexLibrary.tar
  tar xf plexLibrary.tar -C ~plex
  chown -R plex:plex ~plex

# install ocaml fuse
  opam init --auto-setup
  eval `opam config env`
  opam install -y depext
  opam depext -y google-drive-ocamlfuse
  opam install -y ocamlfuse gapi-ocaml sqlite3 google-drive-ocamlfuse
  mv /root/.opam/system/bin/google-drive-ocamlfuse /usr/local/bin/.
  chmod +rx /usr/local/bin/google-drive-ocamlfuse

# parse environment variables
if [[ -z $CONFIG ]]; then CONFIG=/home/media; fi
if [[ -z $TO_ENCODE ]]; then TO_ENCODE=/media/media; fi
if [[ -z $ENCODE_DIR ]]; then ENCODE_DIR=/media/conversion_temp; fi
if [[ -z $UPLOAD_DIR ]]; then UPLOAD_DIR=/media/toUpload; fi
if [[ -z $MOUNTS ]]; then MOUNTS=/media/cloud_mounts; fi
if [[ -z $TOR ]]; then TOR=/media/rTorrent; fi
if [[ -z $LOGD ]]; then LOGD=/media/logs/; fi 

# make users required for system to operate and required folders
  # media user
  useradd -d $CONFIG -m -s /bin/bash media
  usermod -aG docker media
  echo "media   ALL=(ALL:ALL) NOPASSWD:/bin/umount" >> /etc/sudoers
  echo "media   ALL=(ALL:ALL) NOPASSWD:/bin/mount" >> /etc/sudoers

  # make required operational folders
  mkdir -p /media $CONFIG/.ssh
  mkdir -p $MOUNTS $ENCODE_DIR $TO_ENCODE $TOR $UPLOAD_DIR
  cd $TOR            && mkdir downloading comics movies music tv
  cd $UPLOAD_DIR     && mkdir backup comics movies music tv
  cd $MOUNTS         && mkdir gog_vids uni_vids gan_vids
  cd $TO_ENCODE      && mkdir movies tv

  # fix permissions
  mkdir -p $CONFIG/data $CONFIG/data/lock $CONFIG $LOGD
  echo "source /etc/profile.d/media_env.sh" > $CONFIG/.bashrc
  echo "source /etc/profile.d/media_env.sh" > ~plex/.bashrc
  chmod 775 $LOGD && chmod 750 -R $CONFIG
  chown media:plex $LOGD
  chown media:media $ENCODE_DIR $UPLOAD_DIR/* $MOUNTS/* $TOR/* $TO_ENCODE/*
  chown -R media:media $CONFIG

# allow other users to use fuse
echo "user_allow_other" >> /etc/fuse.conf

# define media_env profile file
size=$(df --block-size=1 "$UPLOAD_DIR" | awk 'FNR==2 { print $2 }') 
if [[ -z $UPLOAD_LIMIT ]]; then UPLOAD_LIMIT=$((size/(3*1024*1024*1024))); fi
cat > /etc/profile.d/media_env.sh <<EOL
export LC_ALL="en_US.UTF-8"
export CONFIG=$CONFIG
export TO_ENCODE=$TO_ENCODE
export ENCODE_DIR=$ENCODE_DIR
export UPLOAD_DIR=$UPLOAD_DIR
export MOUNTS=$MOUNTS
export LOGD=$LOGD
export TOR=$TOR

export UPLOAD_LIMIT=$UPLOAD_LIMIT
export DATA=$CONFIG/data
export LOCK=$CONFIG/data/lock
export CLOUD=$MOUNTS/uni_vids
export CONN="mongodb://127.0.0.1:27017/"

export RCLONE_CLOUD="google"
export RCLONE_RPATH=""
EOL
chmod 755 /etc/profile.d/media_env.sh

# cleanup
cd /root/
rm  -rf rclone*
cd /tmp
